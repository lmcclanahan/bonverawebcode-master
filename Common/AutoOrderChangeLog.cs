﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class AutoOrderChangeLog
    {
        [Column("AutoOrderChangeLogID")]
        public int AutoOrderChangeLogId { get; set; }
        [Column("AutoOrderID")]
        public int AutoOrderId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [MaxLength(50)]
        public string ModifiedBy { get; set; }
        [Required]
        [MaxLength(2000)]
        public string Detail { get; set; }
    }
}
