﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemTypes
    {
        [Column("ItemTypeID")]
        [Key]
        public int ItemTypeId { get; set; }
        [MaxLength(50)]
        public string ItemTypeDescription { get; set; }
    }
}
