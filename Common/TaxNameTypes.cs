﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class TaxNameTypes
    {
        [Column("TaxNameTypeID")]
        [Key]
        public int TaxNameTypeId { get; set; }
        [MaxLength(50)]
        public string TaxNameTypeDescription { get; set; }
    }
}
