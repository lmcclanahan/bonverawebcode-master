﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerEventHistory
    {
        [Column("CustomerEventID")]
        public int CustomerEventId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        public int Key1 { get; set; }
        public int Key2 { get; set; }
        public int Key3 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EntryDate { get; set; }
    }
}
