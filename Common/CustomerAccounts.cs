﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerAccounts
    {
        [Column("CustomerID")]
        [Key]
        public int CustomerId { get; set; }
        [MaxLength(50)]
        public string PrimaryCreditCardDisplay { get; set; }
        public int? PrimaryExpirationMonth { get; set; }
        public int? PrimaryExpirationYear { get; set; }
        [Column("PrimaryCreditCardTypeID")]
        public int? PrimaryCreditCardTypeId { get; set; }
        [MaxLength(50)]
        public string PrimaryBillingName { get; set; }
        [MaxLength(100)]
        public string PrimaryBillingAddress { get; set; }
        [MaxLength(50)]
        public string PrimaryBillingCity { get; set; }
        [MaxLength(50)]
        public string PrimaryBillingState { get; set; }
        [MaxLength(50)]
        public string PrimaryBillingZip { get; set; }
        [MaxLength(50)]
        public string PrimaryBillingCountry { get; set; }
        [MaxLength(50)]
        public string SecondaryCreditCardDisplay { get; set; }
        public int? SecondaryExpirationMonth { get; set; }
        public int? SecondaryExpirationYear { get; set; }
        [Column("SecondaryCreditCardTypeID")]
        public int? SecondaryCreditCardTypeId { get; set; }
        [MaxLength(50)]
        public string SecondaryBillingName { get; set; }
        [MaxLength(100)]
        public string SecondaryBillingAddress { get; set; }
        [MaxLength(50)]
        public string SecondaryBillingCity { get; set; }
        [MaxLength(50)]
        public string SecondaryBillingState { get; set; }
        [MaxLength(50)]
        public string SecondaryBillingZip { get; set; }
        [MaxLength(50)]
        public string SecondaryBillingCountry { get; set; }
        [MaxLength(50)]
        public string BankAccountNumber { get; set; }
        [MaxLength(50)]
        public string BankRoutingNumber { get; set; }
        [MaxLength(50)]
        public string BankNameOnAccount { get; set; }
        [MaxLength(100)]
        public string BankAccountAddress { get; set; }
        [MaxLength(50)]
        public string BankAccountCity { get; set; }
        [MaxLength(50)]
        public string BankAccountState { get; set; }
        [MaxLength(50)]
        public string BankAccountZip { get; set; }
        [MaxLength(50)]
        public string BankAccountCountry { get; set; }
        [MaxLength(50)]
        public string DriversLicenseNumber { get; set; }
        [MaxLength(50)]
        public string DepositNameOnAcount { get; set; }
        [MaxLength(50)]
        public string DepositAccountNumber { get; set; }
        [MaxLength(50)]
        public string DepositRoutingNumber { get; set; }
        [MaxLength(50)]
        public string Iban { get; set; }
        [MaxLength(50)]
        public string SwiftCode { get; set; }
        [MaxLength(100)]
        public string DepositBankName { get; set; }
        [MaxLength(250)]
        public string DepositBankAddress { get; set; }
        [MaxLength(50)]
        public string DepositBankCity { get; set; }
        [MaxLength(50)]
        public string DepositBankState { get; set; }
        [MaxLength(50)]
        public string DepositBankZip { get; set; }
        [MaxLength(50)]
        public string DepositBankCountry { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [MaxLength(50)]
        public string ModifiedBy { get; set; }
    }
}
