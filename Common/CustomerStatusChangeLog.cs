﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerStatusChangeLog
    {
        [Column("CustomerStatusChangeLogID")]
        public int CustomerStatusChangeLogId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("CustomerStatusID")]
        public int CustomerStatusId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [MaxLength(50)]
        public string ModifiedBy { get; set; }
    }
}
