﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemDynamicKitCategoryItemMembers
    {
        [Column("DynamicKitCategoryID")]
        public int DynamicKitCategoryId { get; set; }
        [Column("ItemID")]
        public int ItemId { get; set; }
    }
}
