﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemPointAccounts
    {
        [Column("ItemID")]
        public int ItemId { get; set; }
        [Column("PointAccountID")]
        public int PointAccountId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Increment { get; set; }
    }
}
