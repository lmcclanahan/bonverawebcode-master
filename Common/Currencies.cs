﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Currencies
    {
        [MaxLength(3)]
        [Key]
        public string CurrencyCode { get; set; }
        [MaxLength(50)]
        public string CurrencyDescription { get; set; }
        [MaxLength(10)]
        public string CurrencySymbol { get; set; }
    }
}
