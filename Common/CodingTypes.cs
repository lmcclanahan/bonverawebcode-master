﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CodingTypes
    {
        [Column("CodingTypeID")]
        [Key]
        public int CodingTypeId { get; set; }
        [MaxLength(100)]
        public string CodingTypeDescription { get; set; }
    }
}
