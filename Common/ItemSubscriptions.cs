﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemSubscriptions
    {
        [Column("ItemID")]
        public int ItemId { get; set; }
        [Column("SubscriptionID")]
        public int SubscriptionId { get; set; }
        public int? DaysEach { get; set; }
    }
}
