﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionCustomers
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("CustomerTypeID")]
        public int? CustomerTypeId { get; set; }
        [Column("CustomerStatusID")]
        public int? CustomerStatusId { get; set; }
        [Column("RankID")]
        public int? RankId { get; set; }
        [Column("NewRankID")]
        public int? NewRankId { get; set; }
        [Column("PaidRankID")]
        public int? PaidRankId { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
    }
}
