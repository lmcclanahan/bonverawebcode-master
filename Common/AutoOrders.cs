﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class AutoOrders
    {
        [Column("AutoOrderID")]
        [Key]
        public int AutoOrderId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("AutoOrderStatusID")]
        public int? AutoOrderStatusId { get; set; }
        [Column("FrequencyTypeID")]
        public int? FrequencyTypeId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StopDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastRunDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? NextRunDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CancelledDate { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Column("WarehouseID")]
        public int? WarehouseId { get; set; }
        [Column("ShipMethodID")]
        public int? ShipMethodId { get; set; }
        [Column("AutoOrderPaymentTypeID")]
        public int? AutoOrderPaymentTypeId { get; set; }
        [Column("AutoOrderProcessTypeID")]
        public int? AutoOrderProcessTypeId { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string MiddleName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string NameSuffix { get; set; }
        [MaxLength(50)]
        public string Company { get; set; }
        [MaxLength(100)]
        public string Address1 { get; set; }
        [MaxLength(100)]
        public string Address2 { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(50)]
        public string State { get; set; }
        [MaxLength(50)]
        public string Zip { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
        [MaxLength(50)]
        public string County { get; set; }
        [MaxLength(200)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Phone { get; set; }
        [MaxLength(500)]
        public string Notes { get; set; }
        [Column(TypeName = "money")]
        public decimal? Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? SubTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? TaxTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? ShippingTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? DiscountTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? BusinessVolumeTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionableVolumeTotal { get; set; }
        [MaxLength(100)]
        public string AutoOrderDescription { get; set; }
        [MaxLength(400)]
        public string Other11 { get; set; }
        [MaxLength(400)]
        public string Other12 { get; set; }
        [MaxLength(400)]
        public string Other13 { get; set; }
        [MaxLength(400)]
        public string Other14 { get; set; }
        [MaxLength(400)]
        public string Other15 { get; set; }
        [MaxLength(400)]
        public string Other16 { get; set; }
        [MaxLength(400)]
        public string Other17 { get; set; }
        [MaxLength(400)]
        public string Other18 { get; set; }
        [MaxLength(400)]
        public string Other19 { get; set; }
        [MaxLength(400)]
        public string Other20 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [MaxLength(30)]
        public string CreatedBy { get; set; }
        [MaxLength(30)]
        public string ModifiedBy { get; set; }
    }
}
