﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class BillTypes
    {
        [Column("BillTypeID")]
        [Key]
        public int BillTypeId { get; set; }
        [MaxLength(50)]
        public string BillTypeDescription { get; set; }
    }
}
