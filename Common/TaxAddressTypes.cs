﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class TaxAddressTypes
    {
        [Column("TaxAddressTypeID")]
        [Key]
        public int TaxAddressTypeId { get; set; }
        [MaxLength(50)]
        public string TaxAddressTypeDescription { get; set; }
    }
}
