﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class EnrollerTree
    {
        [Column("CustomerID")]
        [Key]
        public int CustomerId { get; set; }
        [Column("EnrollerID")]
        public int? EnrollerId { get; set; }
        public int? NestedLevel { get; set; }
        public int? Lft { get; set; }
        public int? Rgt { get; set; }
    }
}
