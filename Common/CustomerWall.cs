﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerWall
    {
        [Column("CustomerWallItemID")]
        [Key]
        public int CustomerWallItemId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EntryDate { get; set; }
        [MaxLength(2000)]
        public string Text { get; set; }
        [MaxLength(50)]
        public string Field1 { get; set; }
        [MaxLength(50)]
        public string Field2 { get; set; }
        [MaxLength(50)]
        public string Field3 { get; set; }
    }
}
