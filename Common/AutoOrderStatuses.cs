﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class AutoOrderStatuses
    {
        [Column("AutoOrderStatusID")]
        [Key]
        public int AutoOrderStatusId { get; set; }
        [MaxLength(50)]
        public string AutoOrderStatusDescription { get; set; }
    }
}
