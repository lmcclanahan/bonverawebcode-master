﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PriceTypes
    {
        [Column("PriceTypeID")]
        [Key]
        public int PriceTypeId { get; set; }
        [MaxLength(50)]
        public string PriceTypeDescription { get; set; }
    }
}
