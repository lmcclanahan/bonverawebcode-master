﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class AutoOrderStatusChangeLog
    {
        [Column("AutoOrderStatusChangeLogID")]
        public int AutoOrderStatusChangeLogId { get; set; }
        [Column("AutoOrderID")]
        public int AutoOrderId { get; set; }
        [Column("AutoOrderStatusID")]
        public int AutoOrderStatusId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
    }
}
