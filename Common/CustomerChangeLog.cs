﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerChangeLog
    {
        [Column("CustomerChangeLogID")]
        public int CustomerChangeLogId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [MaxLength(100)]
        public string ModifiedBy { get; set; }
        [Required]
        [MaxLength(2000)]
        public string Detail { get; set; }
    }
}
