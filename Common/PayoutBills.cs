﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PayoutBills
    {
        [Column("TransactionID")]
        [Key]
        public int TransactionId { get; set; }
        [Column("PayoutID")]
        public int? PayoutId { get; set; }
        [Column("BillID")]
        public int? BillId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
