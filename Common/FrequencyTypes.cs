﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class FrequencyTypes
    {
        [Column("FrequencyTypeID")]
        [Key]
        public int FrequencyTypeId { get; set; }
        [MaxLength(50)]
        public string FrequencyTypeDescription { get; set; }
    }
}
