﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerExtendedDetails
    {
        [Column("CustomerExtendedDetailID")]
        [Key]
        public int CustomerExtendedDetailId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("CustomerExtendedGroupID")]
        public int? CustomerExtendedGroupId { get; set; }
        [MaxLength(200)]
        public string Field1 { get; set; }
        [MaxLength(200)]
        public string Field2 { get; set; }
        [MaxLength(200)]
        public string Field3 { get; set; }
        [MaxLength(200)]
        public string Field4 { get; set; }
        [MaxLength(200)]
        public string Field5 { get; set; }
        [MaxLength(200)]
        public string Field6 { get; set; }
        [MaxLength(200)]
        public string Field7 { get; set; }
        [MaxLength(200)]
        public string Field8 { get; set; }
        [MaxLength(200)]
        public string Field9 { get; set; }
        [MaxLength(200)]
        public string Field10 { get; set; }
        [MaxLength(200)]
        public string Field11 { get; set; }
        [MaxLength(200)]
        public string Field12 { get; set; }
        [MaxLength(200)]
        public string Field13 { get; set; }
        [MaxLength(200)]
        public string Field14 { get; set; }
        [MaxLength(200)]
        public string Field15 { get; set; }
        [MaxLength(200)]
        public string Field16 { get; set; }
        [MaxLength(200)]
        public string Field17 { get; set; }
        [MaxLength(200)]
        public string Field18 { get; set; }
        [MaxLength(200)]
        public string Field19 { get; set; }
        [MaxLength(200)]
        public string Field20 { get; set; }
        [MaxLength(200)]
        public string Field21 { get; set; }
        [MaxLength(200)]
        public string Field22 { get; set; }
        [MaxLength(200)]
        public string Field23 { get; set; }
        [MaxLength(200)]
        public string Field24 { get; set; }
        [MaxLength(200)]
        public string Field25 { get; set; }
        [MaxLength(200)]
        public string Field26 { get; set; }
        [MaxLength(200)]
        public string Field27 { get; set; }
        [MaxLength(200)]
        public string Field28 { get; set; }
        [MaxLength(200)]
        public string Field29 { get; set; }
        [MaxLength(200)]
        public string Field30 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
