﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PointTransactionTypes
    {
        [Column("PointTransactionTypeID")]
        [Key]
        public int PointTransactionTypeId { get; set; }
        [MaxLength(50)]
        public string PointTransactionTypeDescription { get; set; }
    }
}
