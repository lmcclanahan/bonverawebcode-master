﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class AutoOrderDetails
    {
        [Column("AutoOrderID")]
        public int AutoOrderId { get; set; }
        public int OrderLine { get; set; }
        [Column("ItemID")]
        public int? ItemId { get; set; }
        public string ItemCode { get; set; }
        [MaxLength(255)]
        public string ItemDescription { get; set; }
        [Column(TypeName = "money")]
        public decimal? Quantity { get; set; }
        [Column(TypeName = "money")]
        public decimal? PriceEach { get; set; }
        [Column(TypeName = "money")]
        public decimal? PriceTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? BusinessVolumeEach { get; set; }
        [Column(TypeName = "money")]
        public decimal? BusinessVolume { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionableVolumeEach { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionableVolume { get; set; }
        [Column(TypeName = "money")]
        public decimal? PriceEachOverride { get; set; }
        [Column(TypeName = "money")]
        public decimal? TaxableEachOverride { get; set; }
        [Column(TypeName = "money")]
        public decimal? ShippingPriceEachOverride { get; set; }
        [Column(TypeName = "money")]
        public decimal? BusinessVolumeEachOverride { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionableVolumeEachOverride { get; set; }
        [Column("ParentItemID")]
        public int? ParentItemId { get; set; }
    }
}
