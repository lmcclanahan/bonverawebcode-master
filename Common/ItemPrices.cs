﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemPrices
    {
        [Column("ItemID")]
        public int ItemId { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Column("PriceTypeID")]
        public int PriceTypeId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionableVolume { get; set; }
        [Column(TypeName = "money")]
        public decimal? BusinessVolume { get; set; }
        [Column(TypeName = "money")]
        public decimal? TaxablePrice { get; set; }
        [Column(TypeName = "money")]
        public decimal? ShippingPrice { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other1Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other2Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other3Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other4Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other5Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other6Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other7Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other8Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other9Price { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other10Price { get; set; }
    }
}
