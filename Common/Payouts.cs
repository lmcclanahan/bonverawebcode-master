﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Payouts
    {
        [Column("PayoutID")]
        [Key]
        public int PayoutId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PayoutDate { get; set; }
        [Column("PayoutTypeID")]
        public int? PayoutTypeId { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }
        [MaxLength(101)]
        public string PayeeName { get; set; }
        [MaxLength(50)]
        public string PayeeCompany { get; set; }
        [MaxLength(100)]
        public string PayeeAddress1 { get; set; }
        [MaxLength(50)]
        public string PayeeAddress2 { get; set; }
        [MaxLength(50)]
        public string PayeeCity { get; set; }
        [MaxLength(50)]
        public string PayeeState { get; set; }
        [MaxLength(50)]
        public string PayeeZip { get; set; }
        [MaxLength(50)]
        public string PayeeCountry { get; set; }
        public int? CheckNumber { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CheckDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? VoidedDate { get; set; }
        public int? DepositNumber { get; set; }
        public bool? IsTaxable { get; set; }
        [Column("BankAccountID")]
        public int? BankAccountId { get; set; }
        [MaxLength(50)]
        public string Reference { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [MaxLength(30)]
        public string CreatedBy { get; set; }
        [MaxLength(30)]
        public string ModifiedBy { get; set; }
    }
}
