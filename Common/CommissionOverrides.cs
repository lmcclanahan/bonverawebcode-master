﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionOverrides
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("OverrideID")]
        public int OverrideId { get; set; }
        public bool? Qualifies { get; set; }
        [Column("PeriodTypeID")]
        public int? PeriodTypeId { get; set; }
        [Column("StartPeriodID")]
        public int? StartPeriodId { get; set; }
        [Column("EndPeriodID")]
        public int? EndPeriodId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }
    }
}
