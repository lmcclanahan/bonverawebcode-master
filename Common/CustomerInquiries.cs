﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerInquiries
    {
        [Column("CustomerInquiryID")]
        [Key]
        public int CustomerInquiryId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("CustomerInquiryTypeID")]
        public int CustomerInquiryTypeId { get; set; }
        [Column("CustomerInquiryCategoryID")]
        public int? CustomerInquiryCategoryId { get; set; }
        [Column("CustomerInquiryStatusID")]
        public int CustomerInquiryStatusId { get; set; }
        [Column("ParentID")]
        public int? ParentId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [Required]
        [MaxLength(200)]
        public string Description { get; set; }
        [Required]
        public string Detail { get; set; }
        [MaxLength(50)]
        public string CreatedBy { get; set; }
        [MaxLength(50)]
        public string AssignedTo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ClosedDate { get; set; }
        [MaxLength(50)]
        public string ClosedBy { get; set; }
    }
}
