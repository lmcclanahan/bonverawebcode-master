﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PointTransactions
    {
        [Column("PointTransactionID")]
        [Key]
        public int PointTransactionId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("PointAccountID")]
        public int? PointAccountId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }
        [Column("PointTransactionTypeID")]
        public int? PointTransactionTypeId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? TransactionDate { get; set; }
        [Column("OrderID")]
        public int? OrderId { get; set; }
        [MaxLength(100)]
        public string Reference { get; set; }
    }
}
