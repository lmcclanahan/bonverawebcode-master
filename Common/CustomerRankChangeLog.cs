﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerRankChangeLog
    {
        [Column("CustomerRankChangeLogID")]
        public int CustomerRankChangeLogId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("RankID")]
        public int? RankId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
    }
}
