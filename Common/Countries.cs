﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Countries
    {
        [MaxLength(2)]
        [Key]
        public string CountryCode { get; set; }
        [MaxLength(50)]
        public string CountryDescription { get; set; }
        public int? Priority { get; set; }
    }
}
