﻿using System;
using System.Data.Entity;


namespace Common
{
    public partial class Bonvera_ExigoContext : DbContext
    {
        public virtual DbSet<AutoOrderChangeLog> AutoOrderChangeLog { get; set; }
        public virtual DbSet<AutoOrderDetails> AutoOrderDetails { get; set; }
        public virtual DbSet<AutoOrderPaymentTypes> AutoOrderPaymentTypes { get; set; }
        public virtual DbSet<AutoOrderProcessTypes> AutoOrderProcessTypes { get; set; }
        public virtual DbSet<AutoOrderSchedules> AutoOrderSchedules { get; set; }
        public virtual DbSet<AutoOrderStatusChangeLog> AutoOrderStatusChangeLog { get; set; }
        public virtual DbSet<AutoOrderStatuses> AutoOrderStatuses { get; set; }
        public virtual DbSet<AutoOrders> AutoOrders { get; set; }
        public virtual DbSet<BillStatuses> BillStatuses { get; set; }
        public virtual DbSet<BillTypes> BillTypes { get; set; }
        public virtual DbSet<Bills> Bills { get; set; }
        public virtual DbSet<BinaryPlacementTypes> BinaryPlacementTypes { get; set; }
        public virtual DbSet<BinaryTree> BinaryTree { get; set; }
        public virtual DbSet<Bonuses> Bonuses { get; set; }
        public virtual DbSet<CodedRanks> CodedRanks { get; set; }
        public virtual DbSet<CodingTypes> CodingTypes { get; set; }
        public virtual DbSet<CommissionBinaryTree> CommissionBinaryTree { get; set; }
        public virtual DbSet<CommissionBonuses> CommissionBonuses { get; set; }
        public virtual DbSet<CommissionCustomers> CommissionCustomers { get; set; }
        public virtual DbSet<CommissionDetails> CommissionDetails { get; set; }
        public virtual DbSet<CommissionEnrollerTree> CommissionEnrollerTree { get; set; }
        public virtual DbSet<CommissionExchangeRates> CommissionExchangeRates { get; set; }
        public virtual DbSet<CommissionOverrides> CommissionOverrides { get; set; }
        public virtual DbSet<CommissionRankGroups> CommissionRankGroups { get; set; }
        public virtual DbSet<CommissionRunStatuses> CommissionRunStatuses { get; set; }
        public virtual DbSet<CommissionRuns> CommissionRuns { get; set; }
        public virtual DbSet<CommissionUniLevelTree> CommissionUniLevelTree { get; set; }
        public virtual DbSet<CommissionVolumeTypes> CommissionVolumeTypes { get; set; }
        public virtual DbSet<CommissionVolumes> CommissionVolumes { get; set; }
        public virtual DbSet<Commissions> Commissions { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<CountryRegions> CountryRegions { get; set; }
        public virtual DbSet<CreditCardTypes> CreditCardTypes { get; set; }
        public virtual DbSet<Currencies> Currencies { get; set; }
        public virtual DbSet<CustomerAccountChangeLog> CustomerAccountChangeLog { get; set; }
        public virtual DbSet<CustomerAccounts> CustomerAccounts { get; set; }
        public virtual DbSet<CustomerAdjustments> CustomerAdjustments { get; set; }
        public virtual DbSet<CustomerChangeLog> CustomerChangeLog { get; set; }
        public virtual DbSet<CustomerContacts> CustomerContacts { get; set; }
        public virtual DbSet<CustomerEventHistory> CustomerEventHistory { get; set; }
        public virtual DbSet<CustomerEvents> CustomerEvents { get; set; }
        public virtual DbSet<CustomerExtendedDetails> CustomerExtendedDetails { get; set; }
        public virtual DbSet<CustomerExtendedGroups> CustomerExtendedGroups { get; set; }
        public virtual DbSet<CustomerInquiries> CustomerInquiries { get; set; }
        public virtual DbSet<CustomerInquiryCategories> CustomerInquiryCategories { get; set; }
        public virtual DbSet<CustomerInquiryStatuses> CustomerInquiryStatuses { get; set; }
        public virtual DbSet<CustomerInquiryTypes> CustomerInquiryTypes { get; set; }
        public virtual DbSet<CustomerLeads> CustomerLeads { get; set; }
        public virtual DbSet<CustomerOverrides> CustomerOverrides { get; set; }
        public virtual DbSet<CustomerPayoutSettings> CustomerPayoutSettings { get; set; }
        public virtual DbSet<CustomerPointAccounts> CustomerPointAccounts { get; set; }
        public virtual DbSet<CustomerRankChangeLog> CustomerRankChangeLog { get; set; }
        public virtual DbSet<CustomerSites> CustomerSites { get; set; }
        public virtual DbSet<CustomerSocialNetworks> CustomerSocialNetworks { get; set; }
        public virtual DbSet<CustomerStatusChangeLog> CustomerStatusChangeLog { get; set; }
        public virtual DbSet<CustomerStatuses> CustomerStatuses { get; set; }
        public virtual DbSet<CustomerSubscriptions> CustomerSubscriptions { get; set; }
        public virtual DbSet<CustomerTransactionTypes> CustomerTransactionTypes { get; set; }
        public virtual DbSet<CustomerTypes> CustomerTypes { get; set; }
        public virtual DbSet<CustomerWall> CustomerWall { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<EnrollerTree> EnrollerTree { get; set; }
        public virtual DbSet<FrequencyTypes> FrequencyTypes { get; set; }
        public virtual DbSet<Hash> Hash { get; set; }
        public virtual DbSet<ItemCountryRegions> ItemCountryRegions { get; set; }
        public virtual DbSet<ItemDynamicKitCategories> ItemDynamicKitCategories { get; set; }
        public virtual DbSet<ItemDynamicKitCategoryItemMembers> ItemDynamicKitCategoryItemMembers { get; set; }
        public virtual DbSet<ItemDynamicKitCategoryMembers> ItemDynamicKitCategoryMembers { get; set; }
        public virtual DbSet<ItemLanguages> ItemLanguages { get; set; }
        public virtual DbSet<ItemPointAccounts> ItemPointAccounts { get; set; }
        public virtual DbSet<ItemPrices> ItemPrices { get; set; }
        public virtual DbSet<ItemStaticKitMembers> ItemStaticKitMembers { get; set; }
        public virtual DbSet<ItemSubscriptions> ItemSubscriptions { get; set; }
        public virtual DbSet<ItemTypes> ItemTypes { get; set; }
        public virtual DbSet<ItemWarehouses> ItemWarehouses { get; set; }
        public virtual DbSet<Items> Items { get; set; }
        public virtual DbSet<Languages> Languages { get; set; }
        public virtual DbSet<MerchantDeclineLog> MerchantDeclineLog { get; set; }
        public virtual DbSet<MerchantDeclineReasons> MerchantDeclineReasons { get; set; }
        public virtual DbSet<OrderBatches> OrderBatches { get; set; }
        public virtual DbSet<OrderChangeLog> OrderChangeLog { get; set; }
        public virtual DbSet<OrderDetails> OrderDetails { get; set; }
        public virtual DbSet<OrderStatusChangeLog> OrderStatusChangeLog { get; set; }
        public virtual DbSet<OrderStatuses> OrderStatuses { get; set; }
        public virtual DbSet<OrderTypes> OrderTypes { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Overrides> Overrides { get; set; }
        public virtual DbSet<PaymentTypes> PaymentTypes { get; set; }
        public virtual DbSet<Payments> Payments { get; set; }
        public virtual DbSet<PayoutBills> PayoutBills { get; set; }
        public virtual DbSet<PayoutTypes> PayoutTypes { get; set; }
        public virtual DbSet<Payouts> Payouts { get; set; }
        public virtual DbSet<PeriodRankScores> PeriodRankScores { get; set; }
        public virtual DbSet<PeriodTypes> PeriodTypes { get; set; }
        public virtual DbSet<PeriodVolumes> PeriodVolumes { get; set; }
        public virtual DbSet<Periods> Periods { get; set; }
        public virtual DbSet<PointAccounts> PointAccounts { get; set; }
        public virtual DbSet<PointTransactionTypes> PointTransactionTypes { get; set; }
        public virtual DbSet<PointTransactions> PointTransactions { get; set; }
        public virtual DbSet<PriceTypes> PriceTypes { get; set; }
        public virtual DbSet<Ranks> Ranks { get; set; }
        public virtual DbSet<ShipCarriers> ShipCarriers { get; set; }
        public virtual DbSet<ShipMethods> ShipMethods { get; set; }
        public virtual DbSet<SocialNetworks> SocialNetworks { get; set; }
        public virtual DbSet<Subscriptions> Subscriptions { get; set; }
        public virtual DbSet<TaxAddressTypes> TaxAddressTypes { get; set; }
        public virtual DbSet<TaxCodeTypes> TaxCodeTypes { get; set; }
        public virtual DbSet<TaxNameTypes> TaxNameTypes { get; set; }
        public virtual DbSet<UniLevelTree> UniLevelTree { get; set; }
        public virtual DbSet<WarehouseCurrencies> WarehouseCurrencies { get; set; }
        public virtual DbSet<Warehouses> Warehouses { get; set; }
        public virtual DbSet<WebCategories> WebCategories { get; set; }
        public virtual DbSet<WebCategoryItems> WebCategoryItems { get; set; }

        // Unable to generate entity type for table 'dbo.dbml'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlServer(@"Data Source=ADMIN-PC\SQLEXPRESS;Initial Catalog=bonvera_exigo;Persist Security Info=True;User ID=sa;Password=butter11");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AutoOrderChangeLog>(entity =>
            {
                entity.Property(e => e.AutoOrderChangeLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<AutoOrderDetails>(entity =>
            {
                entity.HasKey(e => new { e.AutoOrderId, e.OrderLine })
                    .HasName("PK_AutoOrderDetails");
            });

            modelBuilder.Entity<AutoOrderPaymentTypes>(entity =>
            {
                entity.Property(e => e.AutoOrderPaymentTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<AutoOrderProcessTypes>(entity =>
            {
                entity.Property(e => e.AutoOrderProcessTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<AutoOrderSchedules>(entity =>
            {
                entity.HasKey(e => new { e.AutoOrderId, e.ScheduledDate })
                    .HasName("PK_AutoOrderSchedules");
            });

            modelBuilder.Entity<AutoOrderStatusChangeLog>(entity =>
            {
                entity.Property(e => e.AutoOrderStatusChangeLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<AutoOrderStatuses>(entity =>
            {
                entity.Property(e => e.AutoOrderStatusId).ValueGeneratedNever();
            });

            modelBuilder.Entity<AutoOrders>(entity =>
            {
                entity.Property(e => e.AutoOrderId).ValueGeneratedNever();
            });

            modelBuilder.Entity<BillStatuses>(entity =>
            {
                entity.Property(e => e.BillStatusId).ValueGeneratedNever();
            });

            modelBuilder.Entity<BillTypes>(entity =>
            {
                entity.Property(e => e.BillTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Bills>(entity =>
            {
                entity.Property(e => e.BillId).ValueGeneratedNever();
            });

            modelBuilder.Entity<BinaryPlacementTypes>(entity =>
            {
                entity.Property(e => e.BinaryPlacementTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<BinaryTree>(entity =>
            {
                entity.Property(e => e.CustomerId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Bonuses>(entity =>
            {
                entity.Property(e => e.BonusId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CodedRanks>(entity =>
            {
                entity.Property(e => e.CodedRankEntryId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CodingTypes>(entity =>
            {
                entity.Property(e => e.CodingTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CommissionBinaryTree>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId })
                    .HasName("PK_CommissionBinaryTree");
            });

            modelBuilder.Entity<CommissionBonuses>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId, e.BonusId })
                    .HasName("PK_CommissionBonuses");
            });

            modelBuilder.Entity<CommissionCustomers>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId })
                    .HasName("PK_CommissionCustomers");
            });

            modelBuilder.Entity<CommissionDetails>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CommissionDetailId })
                    .HasName("PK_CommissionDetails");
            });

            modelBuilder.Entity<CommissionEnrollerTree>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId })
                    .HasName("PK_CommissionEnrollerTree");
            });

            modelBuilder.Entity<CommissionExchangeRates>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CurrencyCode })
                    .HasName("PK_CommissionExchangeRates");
            });

            modelBuilder.Entity<CommissionOverrides>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId, e.OverrideId })
                    .HasName("PK_CommissionOverrides");
            });

            modelBuilder.Entity<CommissionRankGroups>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId, e.RankGroupId })
                    .HasName("PK_CommissionRankGroups");
            });

            modelBuilder.Entity<CommissionRunStatuses>(entity =>
            {
                entity.Property(e => e.CommissionRunStatusId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CommissionRuns>(entity =>
            {
                entity.Property(e => e.CommissionRunId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CommissionUniLevelTree>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId })
                    .HasName("PK_CommissionUniLevelTree");
            });

            modelBuilder.Entity<CommissionVolumeTypes>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.VolumeTypeId })
                    .HasName("PK_CommissionVolumeTypes");
            });

            modelBuilder.Entity<CommissionVolumes>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId, e.VolumeTypeId })
                    .HasName("PK_CommissionVolumes");
            });

            modelBuilder.Entity<Commissions>(entity =>
            {
                entity.HasKey(e => new { e.CommissionRunId, e.CustomerId })
                    .HasName("PK_Commissions");
            });

            modelBuilder.Entity<CountryRegions>(entity =>
            {
                entity.HasKey(e => new { e.CountryCode, e.RegionCode })
                    .HasName("PK_CountryRegions");
            });

            modelBuilder.Entity<CreditCardTypes>(entity =>
            {
                entity.Property(e => e.CreditCardTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerAccountChangeLog>(entity =>
            {
                entity.Property(e => e.CustomerAccountChangeLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerAccounts>(entity =>
            {
                entity.Property(e => e.CustomerId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerAdjustments>(entity =>
            {
                entity.Property(e => e.TransactionId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerChangeLog>(entity =>
            {
                entity.Property(e => e.CustomerChangeLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerContacts>(entity =>
            {
                entity.Property(e => e.CustomerContactId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerEventHistory>(entity =>
            {
                entity.HasKey(e => new { e.CustomerEventId, e.CustomerId, e.Key1, e.Key2, e.Key3 })
                    .HasName("PK_CustomerEventHistory");
            });

            modelBuilder.Entity<CustomerEvents>(entity =>
            {
                entity.Property(e => e.CustomerEventId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerExtendedDetails>(entity =>
            {
                entity.Property(e => e.CustomerExtendedDetailId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerExtendedGroups>(entity =>
            {
                entity.Property(e => e.CustomerExtendedGroupId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerInquiries>(entity =>
            {
                entity.Property(e => e.CustomerInquiryId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerInquiryCategories>(entity =>
            {
                entity.Property(e => e.CustomerInquiryCategoryId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerInquiryStatuses>(entity =>
            {
                entity.Property(e => e.CustomerInquiryStatusId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerInquiryTypes>(entity =>
            {
                entity.Property(e => e.CustomerInquiryTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerLeads>(entity =>
            {
                entity.Property(e => e.CustomerLeadId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerOverrides>(entity =>
            {
                entity.HasKey(e => new { e.PlanId, e.OverrideId, e.CustomerId })
                    .HasName("PK_CustomerOverrides");
            });

            modelBuilder.Entity<CustomerPayoutSettings>(entity =>
            {
                entity.Property(e => e.CustomerId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerPointAccounts>(entity =>
            {
                entity.HasKey(e => new { e.PointAccountId, e.CustomerId })
                    .HasName("PK_CustomerPointAccounts");
            });

            modelBuilder.Entity<CustomerRankChangeLog>(entity =>
            {
                entity.Property(e => e.CustomerRankChangeLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerSites>(entity =>
            {
                entity.Property(e => e.CustomerId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerSocialNetworks>(entity =>
            {
                entity.HasKey(e => new { e.CustomerId, e.SocialNetworkId })
                    .HasName("PK_CustomerSocialNetworks");
            });

            modelBuilder.Entity<CustomerStatusChangeLog>(entity =>
            {
                entity.Property(e => e.CustomerStatusChangeLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerStatuses>(entity =>
            {
                entity.Property(e => e.CustomerStatusId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerSubscriptions>(entity =>
            {
                entity.HasKey(e => new { e.SubscriptionId, e.CustomerId })
                    .HasName("PK_CustomerSubscriptions");
            });

            modelBuilder.Entity<CustomerTransactionTypes>(entity =>
            {
                entity.Property(e => e.CustomerTransactionTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerTypes>(entity =>
            {
                entity.Property(e => e.CustomerTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<CustomerWall>(entity =>
            {
                entity.Property(e => e.CustomerWallItemId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.Property(e => e.CustomerId).ValueGeneratedNever();
            });

            modelBuilder.Entity<EnrollerTree>(entity =>
            {
                entity.Property(e => e.CustomerId).ValueGeneratedNever();
            });

            modelBuilder.Entity<FrequencyTypes>(entity =>
            {
                entity.Property(e => e.FrequencyTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Hash>(entity =>
            {
                entity.Property(e => e.C1).ValueGeneratedNever();
            });

            modelBuilder.Entity<ItemCountryRegions>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.CountryCode, e.RegionCode })
                    .HasName("PK_ItemCountryRegions");
            });

            modelBuilder.Entity<ItemDynamicKitCategories>(entity =>
            {
                entity.Property(e => e.DynamicKitCategoryId).ValueGeneratedNever();
            });

            modelBuilder.Entity<ItemDynamicKitCategoryItemMembers>(entity =>
            {
                entity.HasKey(e => new { e.DynamicKitCategoryId, e.ItemId })
                    .HasName("PK_ItemDynamicKitCategoryItemMembers");
            });

            modelBuilder.Entity<ItemDynamicKitCategoryMembers>(entity =>
            {
                entity.HasKey(e => new { e.MasterItemId, e.DynamicKitCategoryId })
                    .HasName("PK_ItemDynamicKitCategoryMembers");
            });

            modelBuilder.Entity<ItemLanguages>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.LanguageId })
                    .HasName("PK_ItemLanguages");
            });

            modelBuilder.Entity<ItemPointAccounts>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.PointAccountId })
                    .HasName("PK_ItemPointAccounts");
            });

            modelBuilder.Entity<ItemPrices>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.CurrencyCode, e.PriceTypeId })
                    .HasName("PK_ItemPrices");
            });

            modelBuilder.Entity<ItemStaticKitMembers>(entity =>
            {
                entity.HasKey(e => new { e.MasterItemId, e.ItemId })
                    .HasName("PK_ItemStaticKitMembers");
            });

            modelBuilder.Entity<ItemSubscriptions>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.SubscriptionId })
                    .HasName("PK_ItemSubscriptions");
            });

            modelBuilder.Entity<ItemTypes>(entity =>
            {
                entity.Property(e => e.ItemTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<ItemWarehouses>(entity =>
            {
                entity.HasKey(e => new { e.ItemId, e.WarehouseId })
                    .HasName("PK_ItemWarehouses");
            });

            modelBuilder.Entity<Items>(entity =>
            {
                entity.Property(e => e.ItemId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Languages>(entity =>
            {
                entity.Property(e => e.LanguageId).ValueGeneratedNever();
            });

            modelBuilder.Entity<MerchantDeclineLog>(entity =>
            {
                entity.Property(e => e.MerchantDeclineLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<MerchantDeclineReasons>(entity =>
            {
                entity.Property(e => e.MerchantDeclineReasonId).ValueGeneratedNever();
            });

            modelBuilder.Entity<OrderBatches>(entity =>
            {
                entity.HasKey(e => new { e.WarehouseId, e.BatchId, e.PrintedDate })
                    .HasName("PK_OrderBatches");
            });

            modelBuilder.Entity<OrderChangeLog>(entity =>
            {
                entity.Property(e => e.OrderChangeLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<OrderDetails>(entity =>
            {
                entity.HasKey(e => new { e.OrderId, e.OrderLine })
                    .HasName("PK_OrderDetails");
            });

            modelBuilder.Entity<OrderStatusChangeLog>(entity =>
            {
                entity.Property(e => e.OrderStatusChangeLogId).ValueGeneratedNever();
            });

            modelBuilder.Entity<OrderStatuses>(entity =>
            {
                entity.Property(e => e.OrderStatusId).ValueGeneratedNever();
            });

            modelBuilder.Entity<OrderTypes>(entity =>
            {
                entity.Property(e => e.OrderTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.Property(e => e.OrderId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Overrides>(entity =>
            {
                entity.HasKey(e => new { e.PlanId, e.OverrideId })
                    .HasName("PK_Overrides");
            });

            modelBuilder.Entity<PaymentTypes>(entity =>
            {
                entity.Property(e => e.PaymentTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Payments>(entity =>
            {
                entity.Property(e => e.PaymentId).ValueGeneratedNever();
            });

            modelBuilder.Entity<PayoutBills>(entity =>
            {
                entity.Property(e => e.TransactionId).ValueGeneratedNever();
            });

            modelBuilder.Entity<PayoutTypes>(entity =>
            {
                entity.Property(e => e.PayoutTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Payouts>(entity =>
            {
                entity.Property(e => e.PayoutId).ValueGeneratedNever();
            });

            modelBuilder.Entity<PeriodRankScores>(entity =>
            {
                entity.HasKey(e => new { e.PeriodTypeId, e.PeriodId, e.CustomerId, e.PaidRankId })
                    .HasName("PK_PeriodRankScores");
            });

            modelBuilder.Entity<PeriodTypes>(entity =>
            {
                entity.Property(e => e.PeriodTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<PeriodVolumes>(entity =>
            {
                entity.HasKey(e => new { e.PeriodTypeId, e.PeriodId, e.CustomerId })
                    .HasName("PK_PeriodVolumes");
            });

            modelBuilder.Entity<Periods>(entity =>
            {
                entity.HasKey(e => new { e.PeriodTypeId, e.PeriodId })
                    .HasName("PK_Periods");
            });

            modelBuilder.Entity<PointAccounts>(entity =>
            {
                entity.Property(e => e.PointAccountId).ValueGeneratedNever();
            });

            modelBuilder.Entity<PointTransactionTypes>(entity =>
            {
                entity.Property(e => e.PointTransactionTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<PointTransactions>(entity =>
            {
                entity.Property(e => e.PointTransactionId).ValueGeneratedNever();
            });

            modelBuilder.Entity<PriceTypes>(entity =>
            {
                entity.Property(e => e.PriceTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Ranks>(entity =>
            {
                entity.Property(e => e.RankId).ValueGeneratedNever();
            });

            modelBuilder.Entity<ShipCarriers>(entity =>
            {
                entity.Property(e => e.ShipCarrierId).ValueGeneratedNever();
            });

            modelBuilder.Entity<ShipMethods>(entity =>
            {
                entity.Property(e => e.ShipMethodId).ValueGeneratedNever();
            });

            modelBuilder.Entity<SocialNetworks>(entity =>
            {
                entity.Property(e => e.SocialNetworkId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Subscriptions>(entity =>
            {
                entity.Property(e => e.SubscriptionId).ValueGeneratedNever();
            });

            modelBuilder.Entity<TaxAddressTypes>(entity =>
            {
                entity.Property(e => e.TaxAddressTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<TaxCodeTypes>(entity =>
            {
                entity.Property(e => e.TaxCodeTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<TaxNameTypes>(entity =>
            {
                entity.Property(e => e.TaxNameTypeId).ValueGeneratedNever();
            });

            modelBuilder.Entity<UniLevelTree>(entity =>
            {
                entity.Property(e => e.CustomerId).ValueGeneratedNever();
            });

            modelBuilder.Entity<WarehouseCurrencies>(entity =>
            {
                entity.HasKey(e => new { e.WarehouseId, e.CurrencyCode })
                    .HasName("PK_WarehouseCurrencies");
            });

            modelBuilder.Entity<Warehouses>(entity =>
            {
                entity.Property(e => e.WarehouseId).ValueGeneratedNever();
            });

            modelBuilder.Entity<WebCategories>(entity =>
            {
                entity.HasKey(e => new { e.WebId, e.WebCategoryId })
                    .HasName("PK_WebCategories");
            });

            modelBuilder.Entity<WebCategoryItems>(entity =>
            {
                entity.HasKey(e => new { e.WebId, e.WebCategoryId, e.ItemId })
                    .HasName("PK_WebCategoryItems");
            });
        }
    }
}