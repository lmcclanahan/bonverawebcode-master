﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerStatuses
    {
        [Column("CustomerStatusID")]
        [Key]
        public int CustomerStatusId { get; set; }
        [MaxLength(50)]
        public string CustomerStatusDescription { get; set; }
    }
}
