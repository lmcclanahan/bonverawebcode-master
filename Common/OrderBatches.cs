﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class OrderBatches
    {
        [Column("WarehouseID")]
        public int WarehouseId { get; set; }
        [Column("BatchID")]
        public int BatchId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime PrintedDate { get; set; }
        public int? TotalOrders { get; set; }
    }
}
