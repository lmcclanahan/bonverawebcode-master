﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerPayoutSettings
    {
        [Column("CustomerID")]
        [Key]
        public int CustomerId { get; set; }
        public bool? Produce1099 { get; set; }
        [Column("TaxAddressTypeID")]
        public int? TaxAddressTypeId { get; set; }
        [Column("TaxNameTypeID")]
        public int? TaxNameTypeId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [MaxLength(30)]
        public string ModifiedBy { get; set; }
    }
}
