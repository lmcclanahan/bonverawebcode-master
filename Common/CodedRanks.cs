﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CodedRanks
    {
        [Column("CodedRankEntryID")]
        [Key]
        public int CodedRankEntryId { get; set; }
        [Column("CodingTypeID")]
        public int? CodingTypeId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("RankID")]
        public int? RankId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CodedDate { get; set; }
        [Column("CodedToCustomerID")]
        public int? CodedToCustomerId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EntryDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
