﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class WebCategoryItems
    {
        [Column("WebID")]
        public int WebId { get; set; }
        [Column("WebCategoryID")]
        public int WebCategoryId { get; set; }
        [Column("ItemID")]
        public int ItemId { get; set; }
        public int? SortOrder { get; set; }
    }
}
