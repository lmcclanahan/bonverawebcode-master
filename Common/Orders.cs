﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Orders
    {
        [Column("OrderID")]
        [Key]
        public int OrderId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("OrderStatusID")]
        public int? OrderStatusId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? OrderDate { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Column("WarehouseID")]
        public int? WarehouseId { get; set; }
        [Column("ShipMethodID")]
        public int? ShipMethodId { get; set; }
        [Column("OrderTypeID")]
        public int? OrderTypeId { get; set; }
        [Column("PriceTypeID")]
        public int? PriceTypeId { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string MiddleName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string NameSuffix { get; set; }
        [MaxLength(50)]
        public string Company { get; set; }
        [MaxLength(100)]
        public string Address1 { get; set; }
        [MaxLength(100)]
        public string Address2 { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(50)]
        public string State { get; set; }
        [MaxLength(50)]
        public string Zip { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
        [MaxLength(50)]
        public string County { get; set; }
        [MaxLength(200)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Phone { get; set; }
        [MaxLength(500)]
        public string Notes { get; set; }
        [Column(TypeName = "money")]
        public decimal? Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? SubTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? TaxTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? ShippingTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? DiscountTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? DiscountPercent { get; set; }
        [Column(TypeName = "money")]
        public decimal? WeightTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? BusinessVolumeTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionableVolumeTotal { get; set; }
        [MaxLength(50)]
        public string TrackingNumber1 { get; set; }
        [MaxLength(50)]
        public string TrackingNumber2 { get; set; }
        [MaxLength(50)]
        public string TrackingNumber3 { get; set; }
        [MaxLength(50)]
        public string TrackingNumber4 { get; set; }
        [MaxLength(50)]
        public string TrackingNumber5 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other1Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other2Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other3Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other4Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other5Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other6Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other7Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other8Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other9Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other10Total { get; set; }
        [Column(TypeName = "money")]
        public decimal? ShippingTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? OrderTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? FedTaxTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? StateTaxTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? FedShippingTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? StateShippingTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? CityShippingTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? CityLocalShippingTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? CountyShippingTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? CountyLocalShippingTax { get; set; }
        [MaxLength(200)]
        public string Other11 { get; set; }
        [MaxLength(200)]
        public string Other12 { get; set; }
        [MaxLength(200)]
        public string Other13 { get; set; }
        [MaxLength(200)]
        public string Other14 { get; set; }
        [MaxLength(200)]
        public string Other15 { get; set; }
        [MaxLength(200)]
        public string Other16 { get; set; }
        [MaxLength(200)]
        public string Other17 { get; set; }
        [MaxLength(200)]
        public string Other18 { get; set; }
        [MaxLength(200)]
        public string Other19 { get; set; }
        [MaxLength(200)]
        public string Other20 { get; set; }
        public bool? IsCommissionable { get; set; }
        [Column("AutoOrderID")]
        public int? AutoOrderId { get; set; }
        [Column("ReturnOrderID")]
        public int? ReturnOrderId { get; set; }
        [Column("ReplacementOrderID")]
        public int? ReplacementOrderId { get; set; }
        [Column("ParentOrderID")]
        public int? ParentOrderId { get; set; }
        [Column("BatchID")]
        public int? BatchId { get; set; }
        public int? DeclineCount { get; set; }
        [Column("TransferToCustomerID")]
        public int? TransferToCustomerId { get; set; }
        [Column("WebCarrierID1")]
        public int? WebCarrierId1 { get; set; }
        [Column("WebCarrierID2")]
        public int? WebCarrierId2 { get; set; }
        [Column("WebCarrierID3")]
        public int? WebCarrierId3 { get; set; }
        [Column("WebCarrierID4")]
        public int? WebCarrierId4 { get; set; }
        [Column("WebCarrierID5")]
        public int? WebCarrierId5 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ShippedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LockedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [MaxLength(30)]
        public string CreatedBy { get; set; }
        [MaxLength(30)]
        public string ModifiedBy { get; set; }
    }
}
