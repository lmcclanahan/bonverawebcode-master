﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemStaticKitMembers
    {
        [Column("MasterItemID")]
        public int MasterItemId { get; set; }
        [Column("ItemID")]
        public int ItemId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Quantity { get; set; }
    }
}
