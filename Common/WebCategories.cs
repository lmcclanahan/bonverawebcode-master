﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class WebCategories
    {
        [Column("WebID")]
        public int WebId { get; set; }
        [Column("WebCategoryID")]
        public int WebCategoryId { get; set; }
        [Column("ParentID")]
        public int? ParentId { get; set; }
        [MaxLength(50)]
        public string WebCategoryDescription { get; set; }
        public int? NestedLevel { get; set; }
        public int? SortOrder { get; set; }
    }
}
