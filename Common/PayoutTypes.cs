﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PayoutTypes
    {
        [Column("PayoutTypeID")]
        [Key]
        public int PayoutTypeId { get; set; }
        [MaxLength(50)]
        public string PayoutDescription { get; set; }
    }
}
