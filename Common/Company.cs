﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Company
    {
        [MaxLength(15)]
        [Key]
        public string CompanyKey { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
