﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class OrderStatusChangeLog
    {
        [Column("OrderStatusChangeLogID")]
        public int OrderStatusChangeLogId { get; set; }
        [Column("OrderID")]
        public int OrderId { get; set; }
        [Column("OrderStatusID")]
        public int OrderStatusId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [MaxLength(30)]
        public string ModifiedBy { get; set; }
    }
}
