﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class AutoOrderSchedules
    {
        [Column("AutoOrderID")]
        public int AutoOrderId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ScheduledDate { get; set; }
        public bool? IsEnabled { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ProcessedDate { get; set; }
        [Column("OrderID")]
        public int? OrderId { get; set; }
    }
}
