﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class OrderTypes
    {
        [Column("OrderTypeID")]
        [Key]
        public int OrderTypeId { get; set; }
        [MaxLength(50)]
        public string OrderTypeDescription { get; set; }
    }
}
