﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PeriodRankScores
    {
        [Column("PeriodTypeID")]
        public int PeriodTypeId { get; set; }
        [Column("PeriodID")]
        public int PeriodId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("PaidRankID")]
        public int PaidRankId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Score { get; set; }
    }
}
