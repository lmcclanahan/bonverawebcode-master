﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionVolumeTypes
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("VolumeTypeID")]
        public int VolumeTypeId { get; set; }
        [MaxLength(500)]
        public string VolumeTypeDescription { get; set; }
    }
}
