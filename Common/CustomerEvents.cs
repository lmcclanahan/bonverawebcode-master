﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerEvents
    {
        [Column("CustomerEventID")]
        [Key]
        public int CustomerEventId { get; set; }
        [MaxLength(50)]
        public string CustomerEventDescription { get; set; }
        [MaxLength(50)]
        public string Key1Description { get; set; }
        [MaxLength(50)]
        public string Key2Description { get; set; }
        [MaxLength(50)]
        public string Key3Description { get; set; }
        public string Notes { get; set; }
    }
}
