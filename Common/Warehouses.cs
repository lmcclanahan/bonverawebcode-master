﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Warehouses
    {
        [Column("WarehouseID")]
        [Key]
        public int WarehouseId { get; set; }
        [MaxLength(50)]
        public string WarehouseDescription { get; set; }
        [MaxLength(50)]
        public string WarehouseAddress1 { get; set; }
        [MaxLength(50)]
        public string WarehouseAddress2 { get; set; }
        [MaxLength(30)]
        public string WarehouseCity { get; set; }
        [MaxLength(10)]
        public string WarehouseState { get; set; }
        [MaxLength(20)]
        public string WarehouseZip { get; set; }
        [MaxLength(20)]
        public string WarehouseCountry { get; set; }
        [Column("TimeZoneID")]
        public int? TimeZoneId { get; set; }
    }
}
