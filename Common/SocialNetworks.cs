﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class SocialNetworks
    {
        [Column("SocialNetworkID")]
        [Key]
        public int SocialNetworkId { get; set; }
        [MaxLength(100)]
        public string SocialNetworkDescription { get; set; }
    }
}
