﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerInquiryCategories
    {
        [Column("CustomerInquiryCategoryID")]
        [Key]
        public int CustomerInquiryCategoryId { get; set; }
        [MaxLength(50)]
        public string CustomerInquiryCategoryDescription { get; set; }
    }
}
