﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    [Table("hash")]
    public partial class Hash
    {
        [Column("c1")]
        [Key]
        public int C1 { get; set; }
        [Required]
        [Column("c2", TypeName = "nchar(48)")]
        public string C2 { get; set; }
    }
}
