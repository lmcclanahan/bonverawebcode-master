﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerSites
    {
        [Column("CustomerID")]
        [Key]
        public int CustomerId { get; set; }
        [MaxLength(50)]
        public string WebAlias { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(255)]
        public string Company { get; set; }
        [MaxLength(50)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Phone { get; set; }
        [MaxLength(50)]
        public string Phone2 { get; set; }
        [MaxLength(50)]
        public string Fax { get; set; }
        [MaxLength(100)]
        public string Address1 { get; set; }
        [MaxLength(100)]
        public string Address2 { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(50)]
        public string State { get; set; }
        [MaxLength(50)]
        public string Zip { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
        public string Notes1 { get; set; }
        public string Notes2 { get; set; }
        public string Notes3 { get; set; }
        public string Notes4 { get; set; }
        [MaxLength(200)]
        public string Url1 { get; set; }
        [MaxLength(200)]
        public string Url2 { get; set; }
        [MaxLength(200)]
        public string Url3 { get; set; }
        [MaxLength(200)]
        public string Url4 { get; set; }
        [MaxLength(200)]
        public string Url5 { get; set; }
        [MaxLength(200)]
        public string Url6 { get; set; }
        [MaxLength(200)]
        public string Url7 { get; set; }
        [MaxLength(200)]
        public string Url8 { get; set; }
        [MaxLength(200)]
        public string Url9 { get; set; }
        [MaxLength(200)]
        public string Url10 { get; set; }
        [MaxLength(200)]
        public string Url1Description { get; set; }
        [MaxLength(200)]
        public string Url2Description { get; set; }
        [MaxLength(200)]
        public string Url3Description { get; set; }
        [MaxLength(200)]
        public string Url4Description { get; set; }
        [MaxLength(200)]
        public string Url5Description { get; set; }
        [MaxLength(200)]
        public string Url6Description { get; set; }
        [MaxLength(200)]
        public string Url7Description { get; set; }
        [MaxLength(200)]
        public string Url8Description { get; set; }
        [MaxLength(200)]
        public string Url9Description { get; set; }
        [MaxLength(200)]
        public string Url10Description { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
