﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Bonuses
    {
        [Column("BonusID")]
        [Key]
        public int BonusId { get; set; }
        [MaxLength(50)]
        public string BonusDescription { get; set; }
        [Column("PeriodTypeID")]
        public int? PeriodTypeId { get; set; }
    }
}
