﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerExtendedGroups
    {
        [Column("CustomerExtendedGroupID")]
        [Key]
        public int CustomerExtendedGroupId { get; set; }
        [MaxLength(100)]
        public string CustomerExtendedGroupDescription { get; set; }
        [MaxLength(100)]
        public string Field1Name { get; set; }
        [MaxLength(100)]
        public string Field2Name { get; set; }
        [MaxLength(100)]
        public string Field3Name { get; set; }
        [MaxLength(100)]
        public string Field4Name { get; set; }
        [MaxLength(100)]
        public string Field5Name { get; set; }
        [MaxLength(100)]
        public string Field6Name { get; set; }
        [MaxLength(100)]
        public string Field7Name { get; set; }
        [MaxLength(100)]
        public string Field8Name { get; set; }
        [MaxLength(100)]
        public string Field9Name { get; set; }
        [MaxLength(100)]
        public string Field10Name { get; set; }
        [MaxLength(100)]
        public string Field11Name { get; set; }
        [MaxLength(100)]
        public string Field12Name { get; set; }
        [MaxLength(100)]
        public string Field13Name { get; set; }
        [MaxLength(100)]
        public string Field14Name { get; set; }
        [MaxLength(100)]
        public string Field15Name { get; set; }
        [MaxLength(100)]
        public string Field16Name { get; set; }
        [MaxLength(100)]
        public string Field17Name { get; set; }
        [MaxLength(100)]
        public string Field18Name { get; set; }
        [MaxLength(100)]
        public string Field19Name { get; set; }
        [MaxLength(100)]
        public string Field20Name { get; set; }
        [MaxLength(100)]
        public string Field21Name { get; set; }
        [MaxLength(100)]
        public string Field22Name { get; set; }
        [MaxLength(100)]
        public string Field23Name { get; set; }
        [MaxLength(100)]
        public string Field24Name { get; set; }
        [MaxLength(100)]
        public string Field25Name { get; set; }
        [MaxLength(100)]
        public string Field26Name { get; set; }
        [MaxLength(100)]
        public string Field27Name { get; set; }
        [MaxLength(100)]
        public string Field28Name { get; set; }
        [MaxLength(100)]
        public string Field29Name { get; set; }
        [MaxLength(100)]
        public string Field30Name { get; set; }
    }
}
