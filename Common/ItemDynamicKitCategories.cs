﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemDynamicKitCategories
    {
        [Column("DynamicKitCategoryID")]
        [Key]
        public int DynamicKitCategoryId { get; set; }
        [MaxLength(50)]
        public string DynamicKitCategoryDescription { get; set; }
    }
}
