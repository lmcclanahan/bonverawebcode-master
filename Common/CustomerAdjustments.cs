﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerAdjustments
    {
        [Column("TransactionID")]
        [Key]
        public int TransactionId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("CustomerTransactionTypeID")]
        public int? CustomerTransactionTypeId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? TransactionDate { get; set; }
        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [MaxLength(30)]
        public string ModifiedBy { get; set; }
        [MaxLength(50)]
        public string Notes { get; set; }
    }
}
