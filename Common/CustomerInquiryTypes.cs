﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerInquiryTypes
    {
        [Column("CustomerInquiryTypeID")]
        [Key]
        public int CustomerInquiryTypeId { get; set; }
        [MaxLength(50)]
        public string CustomerInquiryTypeDescription { get; set; }
    }
}
