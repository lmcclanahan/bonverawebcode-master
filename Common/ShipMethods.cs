﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ShipMethods
    {
        [Column("ShipMethodID")]
        [Key]
        public int ShipMethodId { get; set; }
        [MaxLength(50)]
        public string ShipMethodDescription { get; set; }
        [Column("WarehouseID")]
        public int? WarehouseId { get; set; }
        [Column("ShipCarrierID")]
        public int? ShipCarrierId { get; set; }
        public bool? DisplayOnWeb { get; set; }
    }
}
