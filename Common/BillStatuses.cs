﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class BillStatuses
    {
        [Column("BillStatusID")]
        [Key]
        public int BillStatusId { get; set; }
        [MaxLength(20)]
        public string BillStatusDescription { get; set; }
    }
}
