﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerPointAccounts
    {
        [Column("PointAccountID")]
        public int PointAccountId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column(TypeName = "money")]
        public decimal? PointBalance { get; set; }
    }
}
