﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Overrides
    {
        [Column("PlanID")]
        public int PlanId { get; set; }
        [Column("OverrideID")]
        public int OverrideId { get; set; }
        [MaxLength(200)]
        public string OverrideDescription { get; set; }
    }
}
