﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class OrderStatuses
    {
        [Column("OrderStatusID")]
        [Key]
        public int OrderStatusId { get; set; }
        [MaxLength(50)]
        public string OrderStatusDescription { get; set; }
    }
}
