﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class AutoOrderPaymentTypes
    {
        [Column("AutoOrderPaymentTypeID")]
        [Key]
        public int AutoOrderPaymentTypeId { get; set; }
        [MaxLength(50)]
        public string AutoOrderPaymentTypeDescription { get; set; }
    }
}
