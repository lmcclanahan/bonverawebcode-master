﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerSocialNetworks
    {
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("SocialNetworkID")]
        public int SocialNetworkId { get; set; }
        [MaxLength(500)]
        public string Url { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
