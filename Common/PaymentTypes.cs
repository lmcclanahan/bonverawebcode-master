﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PaymentTypes
    {
        [Column("PaymentTypeID")]
        [Key]
        public int PaymentTypeId { get; set; }
        [MaxLength(50)]
        public string PaymentTypeDescription { get; set; }
    }
}
