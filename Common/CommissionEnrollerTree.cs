﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionEnrollerTree
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("EnrollerID")]
        public int? EnrollerId { get; set; }
        public int? NestedLevel { get; set; }
        public int? Lft { get; set; }
        public int? Rgt { get; set; }
    }
}
