﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionRunStatuses
    {
        [Column("CommissionRunStatusID")]
        [Key]
        public int CommissionRunStatusId { get; set; }
        [MaxLength(50)]
        public string CommissionRunStatusDescription { get; set; }
    }
}
