﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PointAccounts
    {
        [Column("PointAccountID")]
        [Key]
        public int PointAccountId { get; set; }
        [MaxLength(50)]
        public string PointAccountDescription { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
    }
}
