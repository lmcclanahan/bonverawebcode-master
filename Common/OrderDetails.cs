﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class OrderDetails
    {
        [Column("OrderID")]
        public int OrderId { get; set; }
        public int OrderLine { get; set; }
        [Column("ItemID")]
        public int? ItemId { get; set; }
        [MaxLength(20)]
        public string ItemCode { get; set; }
        [MaxLength(500)]
        public string ItemDescription { get; set; }
        [Column(TypeName = "money")]
        public decimal? Quantity { get; set; }
        [Column(TypeName = "money")]
        public decimal? PriceEach { get; set; }
        [Column(TypeName = "money")]
        public decimal? PriceTotal { get; set; }
        [Column(TypeName = "money")]
        public decimal? Tax { get; set; }
        [Column(TypeName = "money")]
        public decimal? WeightEach { get; set; }
        [Column(TypeName = "money")]
        public decimal? Weight { get; set; }
        [Column(TypeName = "money")]
        public decimal? BusinessVolumeEach { get; set; }
        [Column(TypeName = "money")]
        public decimal? BusinessVolume { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionableVolumeEach { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionableVolume { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other1Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other1 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other2Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other2 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other3Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other3 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other4Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other4 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other5Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other5 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other6Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other6 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other7Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other7 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other8Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other8 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other9Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other9 { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other10Each { get; set; }
        [Column(TypeName = "money")]
        public decimal? Other10 { get; set; }
        [Column("ParentItemID")]
        public int? ParentItemId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Taxable { get; set; }
        [Column(TypeName = "money")]
        public decimal? FedTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? StateTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? CityTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? CityLocalTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? CountyTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? CountyLocalTax { get; set; }
        [Column(TypeName = "money")]
        public decimal? ManualTax { get; set; }
        public bool? IsStateTaxOverride { get; set; }
    }
}
