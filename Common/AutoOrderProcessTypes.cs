﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class AutoOrderProcessTypes
    {
        [Column("AutoOrderProcessTypeID")]
        [Key]
        public int AutoOrderProcessTypeId { get; set; }
        [MaxLength(50)]
        public string AutoOrderProcessTypeDescription { get; set; }
    }
}
