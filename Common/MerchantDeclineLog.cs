﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class MerchantDeclineLog
    {
        [Column("MerchantDeclineLogID")]
        public int MerchantDeclineLogId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime EntryDate { get; set; }
        [Column("MerchantDeclineReasonID")]
        public int MerchantDeclineReasonId { get; set; }
        [Required]
        [MaxLength(500)]
        public string Message { get; set; }
        [Column("MerchantTypeID")]
        public int MerchantTypeId { get; set; }
        [Column(TypeName = "money")]
        public decimal Amount { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("OrderID")]
        public int OrderId { get; set; }
        [Required]
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [MaxLength(50)]
        public string CreditCardDisplay { get; set; }
        public bool? PassedCvvCode { get; set; }
        public int? ExpirationMonth { get; set; }
        public int? ExpirationYear { get; set; }
        [MaxLength(100)]
        public string BillingName { get; set; }
        [MaxLength(100)]
        public string BillingAddress { get; set; }
        [MaxLength(50)]
        public string BillingCity { get; set; }
        [MaxLength(50)]
        public string BillingState { get; set; }
        [MaxLength(50)]
        public string BillingZip { get; set; }
        [MaxLength(50)]
        public string BillingCountry { get; set; }
        [MaxLength(2000)]
        public string AdditionalReturnData { get; set; }
    }
}
