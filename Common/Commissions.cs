﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Commissions
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Column(TypeName = "money")]
        public decimal? Earnings { get; set; }
        [Column(TypeName = "money")]
        public decimal? PreviousBalance { get; set; }
        [Column(TypeName = "money")]
        public decimal? BalanceForward { get; set; }
        [Column(TypeName = "money")]
        public decimal? Fee { get; set; }
        [Column(TypeName = "money")]
        public decimal? Total { get; set; }
    }
}
