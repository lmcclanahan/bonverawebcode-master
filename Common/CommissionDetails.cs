﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionDetails
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("CommissionDetailID")]
        public long CommissionDetailId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("BonusID")]
        public int? BonusId { get; set; }
        [Column("FromCustomerID")]
        public int? FromCustomerId { get; set; }
        [Column("OrderID")]
        public int? OrderId { get; set; }
        [Column(TypeName = "money")]
        public decimal? SourceAmount { get; set; }
        [Column(TypeName = "money")]
        public decimal? Percentage { get; set; }
        [Column(TypeName = "money")]
        public decimal? CommissionAmount { get; set; }
        public int? Level { get; set; }
        public int? PaidLevel { get; set; }
    }
}
