﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CountryRegions
    {
        [MaxLength(2)]
        public string CountryCode { get; set; }
        [MaxLength(3)]
        public string RegionCode { get; set; }
        [MaxLength(50)]
        public string RegionDescription { get; set; }
    }
}
