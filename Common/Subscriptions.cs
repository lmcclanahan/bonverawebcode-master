﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Subscriptions
    {
        [Column("SubscriptionID")]
        [Key]
        public int SubscriptionId { get; set; }
        [MaxLength(50)]
        public string SubscriptionDescription { get; set; }
    }
}
