﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class BinaryPlacementTypes
    {
        [Column("BinaryPlacementTypeID")]
        [Key]
        public int BinaryPlacementTypeId { get; set; }
        [MaxLength(50)]
        public string BinaryPlacementDescription { get; set; }
    }
}
