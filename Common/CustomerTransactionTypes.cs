﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerTransactionTypes
    {
        [Column("CustomerTransactionTypeID")]
        [Key]
        public int CustomerTransactionTypeId { get; set; }
        [MaxLength(50)]
        public string CustomerTransactionTypeDescription { get; set; }
    }
}
