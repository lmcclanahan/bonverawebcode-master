﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class MerchantDeclineReasons
    {
        [Column("MerchantDeclineReasonID")]
        [Key]
        public int MerchantDeclineReasonId { get; set; }
        [MaxLength(50)]
        public string MerchantDeclineReasonDescription { get; set; }
    }
}
