﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Payments
    {
        [Column("PaymentID")]
        [Key]
        public int PaymentId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("OrderID")]
        public int? OrderId { get; set; }
        [Column("PaymentTypeID")]
        public int? PaymentTypeId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? PaymentDate { get; set; }
        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Column("WarehouseID")]
        public int? WarehouseId { get; set; }
        [MaxLength(50)]
        public string BillingName { get; set; }
        [Column("CreditCardTypeID")]
        public int? CreditCardTypeId { get; set; }
        [MaxLength(50)]
        public string CreditCardNumber { get; set; }
        [MaxLength(50)]
        public string AuthorizationCode { get; set; }
        [MaxLength(50)]
        public string CheckNumber { get; set; }
        [MaxLength(500)]
        public string Memo { get; set; }
        [MaxLength(50)]
        public string DriversLicenseNumber { get; set; }
        [Column("MerchantTypeID")]
        public int? MerchantTypeId { get; set; }
        [MaxLength(50)]
        public string MerchantTransactionKey { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
