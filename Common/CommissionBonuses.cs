﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionBonuses
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("BonusID")]
        public int BonusId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }
    }
}
