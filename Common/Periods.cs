﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Periods
    {
        [Column("PeriodTypeID")]
        public int PeriodTypeId { get; set; }
        [Column("PeriodID")]
        public int PeriodId { get; set; }
        [MaxLength(50)]
        public string PeriodDescription { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? EndDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? AcceptedDate { get; set; }
    }
}
