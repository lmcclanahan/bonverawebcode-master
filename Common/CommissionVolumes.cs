﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionVolumes
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("VolumeTypeID")]
        public int VolumeTypeId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Volume { get; set; }
    }
}
