﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Languages
    {
        [Column("LanguageID")]
        [Key]
        public int LanguageId { get; set; }
        [MaxLength(50)]
        public string LanguageDescription { get; set; }
    }
}
