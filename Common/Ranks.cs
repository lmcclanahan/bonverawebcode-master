﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Ranks
    {
        [Column("RankID")]
        [Key]
        public int RankId { get; set; }
        [MaxLength(50)]
        public string RankDescription { get; set; }
    }
}
