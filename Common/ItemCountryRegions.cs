﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemCountryRegions
    {
        [Column("ItemID")]
        public int ItemId { get; set; }
        [MaxLength(2)]
        public string CountryCode { get; set; }
        [MaxLength(3)]
        public string RegionCode { get; set; }
        public bool? UseTaxOverride { get; set; }
        [Column(TypeName = "money")]
        public decimal? TaxOverridePercent { get; set; }
    }
}
