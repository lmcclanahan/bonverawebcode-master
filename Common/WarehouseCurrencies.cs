﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class WarehouseCurrencies
    {
        [Column("WarehouseID")]
        public int WarehouseId { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        public int? Priority { get; set; }
    }
}
