﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemDynamicKitCategoryMembers
    {
        [Column("MasterItemID")]
        public int MasterItemId { get; set; }
        [Column("DynamicKitCategoryID")]
        public int DynamicKitCategoryId { get; set; }
        [Column(TypeName = "money")]
        public decimal? Quantity { get; set; }
    }
}
