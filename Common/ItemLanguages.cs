﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemLanguages
    {
        [Column("ItemID")]
        public int ItemId { get; set; }
        [Column("LanguageID")]
        public int LanguageId { get; set; }
        [MaxLength(255)]
        public string ItemDescription { get; set; }
        [MaxLength(2048)]
        public string ShortDetail { get; set; }
        [MaxLength(2048)]
        public string ShortDetail2 { get; set; }
        [MaxLength(2048)]
        public string ShortDetail3 { get; set; }
        [MaxLength(2048)]
        public string ShortDetail4 { get; set; }
        public string LongDetail { get; set; }
        public string LongDetail2 { get; set; }
        public string LongDetail3 { get; set; }
        public string LongDetail4 { get; set; }
    }
}
