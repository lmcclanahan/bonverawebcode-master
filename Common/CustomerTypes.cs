﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerTypes
    {
        [Column("CustomerTypeID")]
        [Key]
        public int CustomerTypeId { get; set; }
        [MaxLength(50)]
        public string CustomerTypeDescription { get; set; }
        [Column("PriceTypeID")]
        public int? PriceTypeId { get; set; }
    }
}
