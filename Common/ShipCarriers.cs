﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ShipCarriers
    {
        [Column("ShipCarrierID")]
        [Key]
        public int ShipCarrierId { get; set; }
        [MaxLength(50)]
        public string ShipCarrierDescription { get; set; }
    }
}
