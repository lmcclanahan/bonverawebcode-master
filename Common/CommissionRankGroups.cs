﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionRankGroups
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Column("RankGroupID")]
        public int RankGroupId { get; set; }
        [Column("RankID")]
        public int? RankId { get; set; }
        [Column("PaidRankID")]
        public int? PaidRankId { get; set; }
        [Column("LegRankID")]
        public int? LegRankId { get; set; }
    }
}
