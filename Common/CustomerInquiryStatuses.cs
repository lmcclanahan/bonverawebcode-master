﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CustomerInquiryStatuses
    {
        [Column("CustomerInquiryStatusID")]
        [Key]
        public int CustomerInquiryStatusId { get; set; }
        [MaxLength(50)]
        public string CustomerInquiryStatusDescription { get; set; }
    }
}
