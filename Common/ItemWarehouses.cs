﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class ItemWarehouses
    {
        [Column("ItemID")]
        public int ItemId { get; set; }
        [Column("WarehouseID")]
        public int WarehouseId { get; set; }
        public int? MaxAllowedOnOrder { get; set; }
        public int? StockLevel { get; set; }
    }
}
