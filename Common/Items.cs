﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Items
    {
        [Column("ItemID")]
        [Key]
        public int ItemId { get; set; }
        [MaxLength(20)]
        public string ItemCode { get; set; }
        [MaxLength(500)]
        public string ItemDescription { get; set; }
        [Column("ItemTypeID")]
        public int? ItemTypeId { get; set; }
        [MaxLength(2048)]
        public string ShortDetail { get; set; }
        [MaxLength(2048)]
        public string ShortDetail2 { get; set; }
        [MaxLength(2048)]
        public string ShortDetail3 { get; set; }
        [MaxLength(2048)]
        public string ShortDetail4 { get; set; }
        public string LongDetail { get; set; }
        public string LongDetail2 { get; set; }
        public string LongDetail3 { get; set; }
        public string LongDetail4 { get; set; }
        [MaxLength(2048)]
        public string Notes { get; set; }
        [Column(TypeName = "money")]
        public decimal? Weight { get; set; }
        public bool? IsVirtual { get; set; }
        public bool? AllowOnAutoOrder { get; set; }
        public bool? AvailableAllCountryRegions { get; set; }
        [MaxLength(255)]
        public string TinyImageName { get; set; }
        [MaxLength(255)]
        public string SmallImageName { get; set; }
        [MaxLength(255)]
        public string LargeImageName { get; set; }
        [MaxLength(100)]
        public string Field1 { get; set; }
        [MaxLength(100)]
        public string Field2 { get; set; }
        [MaxLength(100)]
        public string Field3 { get; set; }
        [MaxLength(100)]
        public string Field4 { get; set; }
        [MaxLength(100)]
        public string Field5 { get; set; }
        [MaxLength(100)]
        public string Field6 { get; set; }
        [MaxLength(100)]
        public string Field7 { get; set; }
        [MaxLength(100)]
        public string Field8 { get; set; }
        [MaxLength(100)]
        public string Field9 { get; set; }
        [MaxLength(100)]
        public string Field10 { get; set; }
        public bool? OtherCheck1 { get; set; }
        public bool? OtherCheck2 { get; set; }
        public bool? OtherCheck3 { get; set; }
        public bool? OtherCheck4 { get; set; }
        public bool? OtherCheck5 { get; set; }
        [MaxLength(100)]
        public string Auto1 { get; set; }
        [MaxLength(100)]
        public string Auto2 { get; set; }
        [MaxLength(100)]
        public string Auto3 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
