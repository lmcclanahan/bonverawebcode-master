﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionExchangeRates
    {
        [Column("CommissionRunID")]
        public int CommissionRunId { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Column(TypeName = "money")]
        public decimal? Rate { get; set; }
        [Column(TypeName = "money")]
        public decimal? Fee { get; set; }
    }
}
