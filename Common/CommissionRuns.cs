﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CommissionRuns
    {
        [Column("CommissionRunID")]
        [Key]
        public int CommissionRunId { get; set; }
        [MaxLength(100)]
        public string CommissionRunDescription { get; set; }
        [Column("PeriodTypeID")]
        public int? PeriodTypeId { get; set; }
        [Column("PeriodID")]
        public int? PeriodId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RunDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? AcceptedDate { get; set; }
        [Column("CommissionRunStatusID")]
        public int? CommissionRunStatusId { get; set; }
        public bool? HideFromWeb { get; set; }
        [Column("PlanID")]
        public int? PlanId { get; set; }
    }
}
