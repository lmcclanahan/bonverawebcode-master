﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class CreditCardTypes
    {
        [Column("CreditCardTypeID")]
        [Key]
        public int CreditCardTypeId { get; set; }
        [MaxLength(100)]
        public string CreditCardTypeDescription { get; set; }
    }
}
