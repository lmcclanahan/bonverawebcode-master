﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Customers
    {
        [Column("CustomerID")]
        [Key]
        public int CustomerId { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string MiddleName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string NameSuffix { get; set; }
        [MaxLength(50)]
        public string Company { get; set; }
        [Column("CustomerTypeID")]
        public int? CustomerTypeId { get; set; }
        [Column("CustomerStatusID")]
        public int? CustomerStatusId { get; set; }
        [MaxLength(50)]
        public string Email { get; set; }
        [MaxLength(20)]
        public string Phone { get; set; }
        [MaxLength(20)]
        public string Phone2 { get; set; }
        [MaxLength(20)]
        public string MobilePhone { get; set; }
        [MaxLength(20)]
        public string Fax { get; set; }
        [MaxLength(100)]
        public string MainAddress1 { get; set; }
        [MaxLength(50)]
        public string MainAddress2 { get; set; }
        [MaxLength(50)]
        public string MainCity { get; set; }
        [MaxLength(50)]
        public string MainState { get; set; }
        [MaxLength(50)]
        public string MainZip { get; set; }
        [MaxLength(50)]
        public string MainCountry { get; set; }
        [MaxLength(50)]
        public string MainCounty { get; set; }
        [MaxLength(100)]
        public string MailAddress1 { get; set; }
        [MaxLength(50)]
        public string MailAddress2 { get; set; }
        [MaxLength(50)]
        public string MailCity { get; set; }
        [MaxLength(50)]
        public string MailState { get; set; }
        [MaxLength(50)]
        public string MailZip { get; set; }
        [MaxLength(50)]
        public string MailCountry { get; set; }
        [MaxLength(50)]
        public string MailCounty { get; set; }
        [MaxLength(100)]
        public string OtherAddress1 { get; set; }
        [MaxLength(50)]
        public string OtherAddress2 { get; set; }
        [MaxLength(50)]
        public string OtherCity { get; set; }
        [MaxLength(50)]
        public string OtherState { get; set; }
        [MaxLength(50)]
        public string OtherZip { get; set; }
        [MaxLength(50)]
        public string OtherCountry { get; set; }
        [MaxLength(50)]
        public string OtherCounty { get; set; }
        public bool? CanLogin { get; set; }
        [MaxLength(100)]
        public string LoginName { get; set; }
        [MaxLength(50)]
        public byte[] PasswordHash { get; set; }
        [Column("RankID")]
        public int? RankId { get; set; }
        [Column("EnrollerID")]
        public int? EnrollerId { get; set; }
        [Column("SponsorID")]
        public int? SponsorId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? BirthDate { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [MaxLength(50)]
        public string PayableToName { get; set; }
        [Column("DefaultWarehouseID")]
        public int? DefaultWarehouseId { get; set; }
        [Column("PayableTypeID")]
        public int? PayableTypeId { get; set; }
        [Column(TypeName = "money")]
        public decimal? CheckThreshold { get; set; }
        [Column("LanguageID")]
        public int? LanguageId { get; set; }
        [MaxLength(1)]
        public string Gender { get; set; }
        [MaxLength(50)]
        public string TaxCode { get; set; }
        [Column("TaxCodeTypeID")]
        public int? TaxCodeTypeId { get; set; }
        public bool? IsSalesTaxExempt { get; set; }
        [MaxLength(50)]
        public string SalesTaxCode { get; set; }
        [MaxLength(50)]
        public string VatRegistration { get; set; }
        [Column("BinaryPlacementTypeID")]
        public int? BinaryPlacementTypeId { get; set; }
        public bool? UseBinaryHoldingTank { get; set; }
        public bool? IsInBinaryHoldingTank { get; set; }
        public bool? IsEmailSubscribed { get; set; }
        [Column("EmailSubscribeIP")]
        [MaxLength(50)]
        public string EmailSubscribeIp { get; set; }
        public string Notes { get; set; }
        [MaxLength(100)]
        public string Field1 { get; set; }
        [MaxLength(100)]
        public string Field2 { get; set; }
        [MaxLength(100)]
        public string Field3 { get; set; }
        [MaxLength(100)]
        public string Field4 { get; set; }
        [MaxLength(100)]
        public string Field5 { get; set; }
        [MaxLength(100)]
        public string Field6 { get; set; }
        [MaxLength(100)]
        public string Field7 { get; set; }
        [MaxLength(100)]
        public string Field8 { get; set; }
        [MaxLength(100)]
        public string Field9 { get; set; }
        [MaxLength(100)]
        public string Field10 { get; set; }
        [MaxLength(100)]
        public string Field11 { get; set; }
        [MaxLength(100)]
        public string Field12 { get; set; }
        [MaxLength(100)]
        public string Field13 { get; set; }
        [MaxLength(100)]
        public string Field14 { get; set; }
        [MaxLength(100)]
        public string Field15 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date1 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date2 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date3 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date4 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date5 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [MaxLength(30)]
        public string CreatedBy { get; set; }
        [MaxLength(30)]
        public string ModifiedBy { get; set; }
    }
}
