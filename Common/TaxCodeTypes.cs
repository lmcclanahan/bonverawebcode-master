﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class TaxCodeTypes
    {
        [Column("TaxCodeTypeID")]
        [Key]
        public int TaxCodeTypeId { get; set; }
        [MaxLength(50)]
        public string TaxCodeDescription { get; set; }
    }
}
