﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class Bills
    {
        [Column("BillID")]
        [Key]
        public int BillId { get; set; }
        [Column("CustomerID")]
        public int? CustomerId { get; set; }
        [Column("BillStatusID")]
        public int? BillStatusId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DueDate { get; set; }
        [Column("BillTypeID")]
        public int? BillTypeId { get; set; }
        [MaxLength(3)]
        public string CurrencyCode { get; set; }
        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }
        [Column("CommissionRunID")]
        public int? CommissionRunId { get; set; }
        public bool? IsOtherIncome { get; set; }
        [MaxLength(50)]
        public string Reference { get; set; }
        [MaxLength(500)]
        public string Notes { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
        [MaxLength(30)]
        public string CreatedBy { get; set; }
        [MaxLength(30)]
        public string ModifiedBy { get; set; }
    }
}
