﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class PeriodTypes
    {
        [Column("PeriodTypeID")]
        [Key]
        public int PeriodTypeId { get; set; }
        [MaxLength(50)]
        public string PeriodTypeDescription { get; set; }
    }
}
