﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common
{
    public partial class OrderChangeLog
    {
        [Column("OrderChangeLogID")]
        public int OrderChangeLogId { get; set; }
        [Column("OrderID")]
        public int OrderId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ModifiedDate { get; set; }
        [Required]
        [MaxLength(50)]
        public string ModifiedBy { get; set; }
        [Required]
        [MaxLength(2000)]
        public string Detail { get; set; }
    }
}
